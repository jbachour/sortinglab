package sortersTesterClasses;
import java.util.Comparator;
import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;

public class ArrayTester {

    static Integer[] array = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
    
    public static void main(String[] args) {
    	showArray("Original Array", array);
        test("Sorting Using IntegerComparator1", new IntegerComparator1());
        test("Sorting Using IntegerComparator2", new IntegerComparator2());

    }
    
    
    public static void test(String mensaje, Comparator<Integer> compare) {



        
        System.out.println("\n\n*******************************************************");
        System.out.println("*** " + mensaje + "  ***");
        System.out.println("*******************************************************");
        

            Sorter<Integer> sorter = new BubbleSortSorter<Integer>();
            sorter.sort(array, compare);
            showArray(sorter.getName() + ": ", array); 
        
}
    

    private static void showArray(String mensaje, Integer[] b) {
        System.out.print(mensaje); 
        for (int i=0; i<b.length; i++) 
            System.out.print("\t" + b[i]); 
        System.out.println();
    }

}
