package sortersTesterClasses;
import java.util.Comparator;
import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;

public class EnteroComparator {
 
    public static void main(String[] args) {
        test("Sorting Using DefaultComp", null);
        

    }
    
    public static void test(String mensaje, Comparator<Entero> compare) {
        Entero[] array = new Entero[5];
        array[0] = new Entero(7);
        array[1] = new Entero(1);
        array[2] = new Entero(3);
        array[3] = new Entero(2);
        array[4] = new Entero(6);

        
        System.out.println("\n\n*******************************************************");
        System.out.println("*** " + mensaje + "  ***");
        System.out.println("*******************************************************");
        
            Sorter<Entero> sorter = new BubbleSortSorter<Entero>(); 
            sorter.sort(array, compare);
            showArray(sorter.getName() + ": ", array); 
      
}
    

    private static void showArray(String mensaje, Entero[] b) {
        System.out.print(mensaje); 
        for (int i=0; i<b.length; i++) 
            System.out.print("\t" + b[i]); 
        System.out.println();
    }

}
